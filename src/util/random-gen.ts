
enum ResultType {
    Fumble = -2,
    Fail = -1,
    Success = 0,
    HardSuccess = 1,
    ExtremeSuccess = 2,
    CriticalSuccess = 3
}

export class RollResult {
    public diceString: string = "";
    public diceResult: number[] = [];
    public diceCount: number[] = [];
    public diceSize: number[] = [];
    public totalResult: number = 0;
    public modifier: number = 0;
    public resultType: ResultType = 0;
}

export function getRollResultAsString(value: ResultType){
    return Object.keys(ResultType)[Object.values(ResultType).indexOf(value)].toString().replace(/([A-Z])/g, ' $1').trim();
}

export class RandomGenerator {

    public randomNumber(min: number = 0, max: number = 1): number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}



export class RandomDice extends RandomGenerator {

    // private parseArgument(input: RollResult, filter: string): RollResult{
    //     if (input.diceString?.includes(filter)){

    //     }
    // }

    // public parseSimpleDiceString(dice: string): number {
    //     let rollResult: RollResult = {
    //         diceString: dice
    //     };

    //     if(dice.includes("+")){

    //     }

    //     const resultArr = dice.split("d")
    //     if (resultArr.length < 1) {
    //         return 0;
    //     }

    //     let result = 0
    //     for (let i = 0; i < +resultArr[0]; i++) {
    //         result += this.randomNumber(1, +resultArr[1]);
    //     }
    //     return result
    // }

    isSuccess(result: number, threshold: number): ResultType {
        if (result >= 96)
            return ResultType.Fumble
        else if (result > threshold) 
            return ResultType.Fail
        else if (result === 1)
            return ResultType.CriticalSuccess
        else if (result <= Math.floor(threshold/5))
            return ResultType.ExtremeSuccess
        else if (result <= Math.floor(threshold/2))
            return ResultType.HardSuccess
        else if (result <= threshold)
            return ResultType.Success
        else
            return ResultType.Success
    }

    public skillCheck(threshold: number) {
        let number = this.randomNumber(1, 100)
        let successType = this.isSuccess(number, threshold)
        const rollResult: RollResult = {
            totalResult: number,
            diceCount: [1],
            diceSize: [100],
            diceResult: [number],
            resultType: successType,
            diceString: "",
            modifier: 0
        };
        return rollResult;
    }
}
