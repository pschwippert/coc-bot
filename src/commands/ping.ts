import { ApplicationCommandType, Client, CommandInteraction, GuildMember, SlashCommandBuilder, User } from "discord.js"
import { Command } from "./base-command"

export const Ping: Command = {
    name: "ping",
    description: "Will do a quick ping pong",
    type: ApplicationCommandType.ChatInput,
    run: async (client: Client, interaction: CommandInteraction) => {
        const content = "Pong!";

        await interaction.followUp({
            ephemeral: true,
            content
        })
    }
}
