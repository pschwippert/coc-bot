import { Command } from "./base-command";
import { Check } from "./check";
import { Ping } from "./ping";

export const Commands: Command[] = [
    Ping, Check
];