import { ApplicationCommandType, Client, CommandInteraction, ApplicationCommandNumericOptionData, CommandOptionNumericResolvableType, ApplicationCommandOptionType } from "discord.js"
import { getRollResultAsString, RandomDice, RollResult } from "../../src/util/random-gen";
import { Command } from "./base-command"

export const Check: Command = {
    name: "check",
    description: "Performs a skill check for the added threshold",
    type: ApplicationCommandType.ChatInput,
    options: [
        {
            name: "threshold",
            description: "The amount of points you have in this skill",
            type: ApplicationCommandOptionType.Number, 
            required: true
        } as ApplicationCommandNumericOptionData
        ],
    run: async (client: Client, interaction: CommandInteraction) => {
        const threshold = interaction.options.data.find(x => x.name === "threshold")?.value ?? 0;        
        const result: RollResult = new RandomDice().skillCheck(+threshold);
        const content = `You rolled a \`${result.totalResult}\` with a DC of \`${threshold}\` which is a(n) \`${getRollResultAsString(result.resultType)}\`!`;

        await interaction.followUp({
            ephemeral: true,
            content
        })
    }
}
